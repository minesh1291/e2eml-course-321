"""
Animate the convolution of one-dimensional signals with kernels.

The convention used here is that the kernel is the shorter of the two, but
the math doesn't care which is which.

This makes use of the Lodgepole image processing package:
https://gitlab.com/brohrer/lodgepole

and FFMPEG:
https://ffmpeg.org/

parameter dictionary
All are floats, in units of centimeters
----------
dy_figure: figure height
dx_figure: figure dx_figureth
dx_figure_border: horizontal border width
dx_labels: label column width
dy_math_text: height of math text
dx_array_element_spacing: width of default spacing between points
dy_lolli_plots: height of the signal plots
dx_lolli_plot_area: width of the signal plots
dy_figure_border: vertical border width
"""
import os
import numpy as np
import matplotlib.pyplot as plt
import lodgepole.animation_tools as at
plt.switch_backend("agg")


# Valid Matplotlib colors. More examples here:
# https://e2eml.school/matplotlib_lines.html#color
canvas_color = "#fbf9f6"  # warm white
kernel_color = "#050560"
signal_color = "#600505"
result_color = "#04253a"  # dark blue
math_color = "#a0a0a0"  # light gray
print_color = "#04253a"

dpi = 300  # Image resolution, dots per inch
results_dir = "results"
frames_dir = "frames"

# Ensure that the necessary directories exist
try:
    os.mkdir(results_dir)
except FileExistsError:
    pass
try:
    os.mkdir(frames_dir)
except FileExistsError:
    pass


def get_empty_frame(
    dx_figure=16,  # centimeters
    dy_figure=9,  # centimeters
    canvas_color="white",
    border_color="black",
):
    """
    Generate a blank canvas on which to draw
    """
    # figsize expects inches. Convert from centimeters.
    figure = plt.figure(figsize=(dx_figure / 2.54, dy_figure / 2.54))

    # Cover the entire figure with an Axes object
    figure_axes = figure.add_axes((0, 0, 1, 1))

    # Ensure that 1 unit in the Axes is 1 centimeter in the final image
    figure_axes.set_xlim(0, dx_figure)
    figure_axes.set_ylim(0, dy_figure)

    figure_axes.set_facecolor(canvas_color)

    # Clean up the axes
    figure_axes.tick_params(
        bottom=False,
        top=False,
        left=False,
        right=False)
    figure_axes.tick_params(
        labelbottom=False,
        labeltop=False,
        labelleft=False,
        labelright=False)

    # Create a border
    figure_axes.spines["top"].set_color(border_color)
    figure_axes.spines["bottom"].set_color(border_color)
    figure_axes.spines["left"].set_color(border_color)
    figure_axes.spines["right"].set_color(border_color)
    figure_axes.spines["top"].set_linewidth(4)
    figure_axes.spines["bottom"].set_linewidth(4)
    figure_axes.spines["left"].set_linewidth(4)
    figure_axes.spines["right"].set_linewidth(4)

    return figure, figure_axes


def calculate_parameters(n_signal_elements, n_kernel_elements):
    # Overall figure size
    dx_figure = 16
    dy_figure = 9

    # A little whitespace around the edges
    dx_figure_border = .5
    dy_figure_border = .5

    # Some room for text labels on the right hand edge
    dx_labels = .75

    # Some room for math symbols between plots
    dy_math_text = .4

    # Calculate the size of the plots
    dy_lolli_plots = (dy_figure - dy_figure_border * 2 - dy_math_text * 3) / 4
    dx_lolli_plot_area = dx_figure - dx_figure_border * 2 - dx_labels

    # Calculate the spacing between points
    dx_array_element_spacing_min = .5  # The minimum spacing allowed between points
    dx_array_element_spacing = np.minimum(
        dx_array_element_spacing_min,
        dx_lolli_plot_area / (
            n_signal_elements + 2 * (n_kernel_elements - 1)))

    return (
        dx_figure,
        dy_figure,
        dx_figure_border,
        dx_labels,
        dy_math_text,
        dx_array_element_spacing,
        dx_lolli_plot_area,
        dy_lolli_plots,
        dy_figure_border
    )


def plot_lolli(
    figure_axes,
    array,
    x_lolli_plot_center,
    y_lolli_plot_axis,
    dx_array_element_spacing,
    dy_lolli_plots,
    color="black",
):
    """
    Create a lollipop-style plot of a one-dimensional array
    """
    dx_lolli_plot = array.size * dx_array_element_spacing
    x_lolli_plot_left = x_lolli_plot_center - dx_lolli_plot / 2
    x_lolli_plot_right = x_lolli_plot_center + dx_lolli_plot / 2

    # Scale the array to fill the allotted vertical space.
    # Assumes that the array has a maximum magnitude of 1
    array *= dy_lolli_plots / 2

    figure_axes.plot(
        [x_lolli_plot_left, x_lolli_plot_right],
        [y_lolli_plot_axis, y_lolli_plot_axis],
        color=color,
        linewidth=1,
    )

    for i_element, element_value in enumerate(array):
        x_element = (
            x_lolli_plot_left + (i_element + .5) * dx_array_element_spacing)
        y_element = y_lolli_plot_axis + element_value
        figure_axes.plot(
            [x_element, x_element],
            [y_lolli_plot_axis, y_element],
            color=color,
            linewidth=.5,
        )
        figure_axes.scatter(
            x_element, y_element,
            marker="o",
            edgecolor=color,
            facecolor=color,
            s=20)


def conv_diagram(
    signal,
    kernel,
    position,
    n_image_copies=1,
    output_directory_name=".",
    filename_stem=None,
    show_math=True,
):
    """
    Create an image showing the convolution.
    """
    if filename_stem is None:
        filename_stem = "frame"

    # Scale to have maximum magnitude of 1
    # This makes for a good visual presentation
    kernel /= np.max(np.abs(kernel))
    signal /= np.max(np.abs(signal))

    (dx_figure,
        dy_figure,
        dx_figure_border,
        dx_labels,
        dy_math_text,
        dx_array_element_spacing,
        dx_lolli_plot_area,
        dy_lolli_plots,
        dy_figure_border) = calculate_parameters(signal.size, kernel.size)

    figure, figure_axes = get_empty_frame(
        dx_figure=dx_figure,
        dy_figure=dy_figure,
        canvas_color=canvas_color,
        border_color=print_color,
    )

    def place_label(y_label_text, label):
        x_label_text = dx_figure - dx_figure_border - dx_labels / 2
        figure_axes.text(
            x_label_text, y_label_text,
            label,
            horizontalalignment="center",
            verticalalignment="center",
            rotation=90,
            color=print_color,
            fontsize=8,
        )

    # Plot the kernel
    y_kernel_plot_axis = (
        dy_figure_border
        + 3.5 * dy_lolli_plots
        + 3 * dy_math_text
    )
    x_lolli_plot_center = (
        dx_figure_border
        + .5 * dx_lolli_plot_area
    )
    plot_lolli(
        figure_axes,
        kernel,
        x_lolli_plot_center,
        y_kernel_plot_axis,
        dx_array_element_spacing,
        dy_lolli_plots,
        color=kernel_color,
    )
    place_label(y_kernel_plot_axis, "kernel")

    # Plot the signal
    y_signal_plot_axis = (
        dy_figure_border
        + 2.5 * dy_lolli_plots
        + 2 * dy_math_text
    )
    plot_lolli(
        figure_axes,
        signal,
        x_lolli_plot_center,
        y_signal_plot_axis,
        dx_array_element_spacing,
        dy_lolli_plots,
        color=signal_color,
    )
    place_label(y_signal_plot_axis, "signal")

    # Plot the padding delimeters
    n_half_kernel = int((kernel.size - 1) / 2)
    x_padded_signal_plot_first_element = (
        x_lolli_plot_center
        - ((signal.size - 1)
            * dx_array_element_spacing) / 2
    )
    x_padded_signal_plot_last_element = (
        x_lolli_plot_center
        + ((signal.size - 1)
            * dx_array_element_spacing) / 2
    )
    x_unpadded_signal_plot_first_element = (
        x_padded_signal_plot_first_element
        + n_half_kernel
        * dx_array_element_spacing
    )
    x_unpadded_signal_plot_last_element = (
        x_padded_signal_plot_last_element
        - n_half_kernel
        * dx_array_element_spacing
    )
    x_left_signal_plot_pad_boundary = (
        x_unpadded_signal_plot_first_element
        - .5 * dx_array_element_spacing
    )
    x_right_signal_plot_pad_boundary = (
        x_unpadded_signal_plot_last_element
        + .5 * dx_array_element_spacing
    )
    figure_axes.plot(
        [x_left_signal_plot_pad_boundary, x_left_signal_plot_pad_boundary],
        [y_signal_plot_axis - dy_lolli_plots / 2,
            y_signal_plot_axis + dy_lolli_plots / 2],
        linestyle="--",
        linewidth=.5,
        color="gray",
    )
    figure_axes.plot(
        [x_right_signal_plot_pad_boundary, x_right_signal_plot_pad_boundary],
        [y_signal_plot_axis - dy_lolli_plots / 2,
            y_signal_plot_axis + dy_lolli_plots / 2],
        linestyle="--",
        linewidth=.5,
        color="gray",
    )

    dx_pad_rectangle = (
        x_left_signal_plot_pad_boundary
        - x_padded_signal_plot_first_element
        + .5 * dx_array_element_spacing
    )
    figure_axes.add_patch(plt.Rectangle(
        (x_padded_signal_plot_first_element - .5 * dx_array_element_spacing,
            y_signal_plot_axis - dy_lolli_plots / 2),
        dx_pad_rectangle,
        dy_lolli_plots,
        ec="None",
        fc="gray",
        alpha=.3))

    figure_axes.add_patch(plt.Rectangle(
        (x_right_signal_plot_pad_boundary,
            y_signal_plot_axis - dy_lolli_plots / 2),
        dx_pad_rectangle,
        dy_lolli_plots,
        ec="None",
        fc="gray",
        alpha=.3))

    # Plot the sliding kernel
    x_sliding_kernel_first_element = (
        x_padded_signal_plot_first_element
        + dx_array_element_spacing * position
    )
    x_sliding_kernel_center = (
        x_sliding_kernel_first_element
        + n_half_kernel * dx_array_element_spacing
    )
    y_kernel_plot_axis = (
        dy_figure_border
        + 1.5 * dy_lolli_plots
        + dy_math_text
    )
    plot_lolli(
        figure_axes,
        kernel[::-1],
        x_sliding_kernel_center,
        y_kernel_plot_axis,
        dx_array_element_spacing,
        dy_lolli_plots,
        color=kernel_color,
    )
    place_label(y_kernel_plot_axis, "convolution")

    # Plot the result
    result_array = np.convolve(signal, kernel, mode="valid")
    result_array /= np.max(np.abs(result_array))
    dx_result_plot_axis = (position + 1) * dx_array_element_spacing
    x_result_plot_first_element = x_unpadded_signal_plot_first_element
    x_result_plot_last_element = (
        x_result_plot_first_element
        + position * dx_array_element_spacing
    )
    x_result_plot_center = (
        x_result_plot_first_element
        + x_result_plot_last_element) / 2

    y_result_plot_axis = dy_figure_border + .5 * dy_lolli_plots
    plot_lolli(
        figure_axes,
        result_array[:position + 1],
        x_result_plot_center,
        y_result_plot_axis,
        dx_array_element_spacing,
        dy_lolli_plots,
        color=result_color,
    )
    place_label(y_result_plot_axis, "result")

    if show_math:
        # Show the multiplications
        y_multiplication_text = (
            dy_figure_border
            + 2 * dy_lolli_plots
            + 1.5 * dy_math_text
        )
        for k_element in range(kernel.size):
            x_multiplication_text = (
                x_sliding_kernel_first_element
                + k_element * dx_array_element_spacing
                # + (kernel.size - 1) * dx_array_element_spacing
            )
            # Add an epsilon to account for floating point errors
            epsilon = 1e-6
            if (x_multiplication_text
                >= (x_padded_signal_plot_first_element - epsilon)
            ) and (x_multiplication_text
                <= (x_padded_signal_plot_last_element + epsilon)
            ):
                figure_axes.text(
                    x_multiplication_text,
                    y_multiplication_text,
                    "X",
                    fontsize=6,
                    color=math_color,
                    horizontalalignment="center",
                    verticalalignment="center",
                )
                # This line extends down toward the addition
                figure_axes.plot(
                    [x_multiplication_text, x_multiplication_text],
                    [y_multiplication_text
                        + .5 * dy_lolli_plots
                        + .5 * dy_math_text,
                        y_multiplication_text
                        - dy_lolli_plots
                        - .5 * dy_math_text],
                    color=math_color,
                    linewidth=.5,
                    alpha=.5,
                    zorder=-4,
                )
                figure_axes.plot(
                    [x_multiplication_text, x_multiplication_text],
                    [y_multiplication_text + dy_math_text / 4,
                        y_multiplication_text - dy_math_text / 4],
                    color=canvas_color,
                    linewidth=.5,
                    zorder=-3,
                )

        # Show the addition
        y_addition_text = dy_figure_border + dy_lolli_plots + .5 * dy_math_text
        x_addition_text = x_sliding_kernel_center
        figure_axes.text(
            x_addition_text,
            y_addition_text,
            "+",
            fontsize=8,
            color=math_color,
            horizontalalignment="center",
            verticalalignment="center",
        )
        figure_axes.plot(
            [x_addition_text, x_addition_text],
            [y_addition_text - .5 * dy_math_text,
                y_addition_text - .5 * (dy_lolli_plots + dy_math_text)],
            color=math_color,
            linewidth=.5,
            alpha=.5,
            zorder=-4,
        )

    # Generate the images to be used as video frames
    # Simulate a slower frame rate by creating copies of each image
    for i_frame in np.arange(
        position * n_image_copies,
        (position + 1) * n_image_copies
    ):
        imagename = filename_stem + str(int(10000 + i_frame)) + ".png"
        figure.savefig(os.path.join(output_directory_name, imagename), dpi=dpi)

    plt.close()


def conv_video(sig, ker, filename_base, fps=30):
    """
    Animate the process of convolution
    """
    # Clean out previous frames
    files = os.listdir(frames_dir)
    for f in files:
        os.remove(os.path.join(frames_dir, f))
    # Calculate how many times each image needs to be repeated to get
    # the desired frame rate
    n_frame_reps = int(np.ceil(30 / fps))

    filename = filename_base + ".mp4"
    signal = np.array(sig, dtype=float)
    kernel = np.array(ker, dtype=float)
    n_conv = signal.size - kernel.size + 1

    # Step through the convolution
    for position in np.arange(n_conv):
        conv_diagram(
            signal,
            kernel,
            position,
            n_image_copies=n_frame_reps,
            output_directory_name=frames_dir,
        )

    at.render_movie(
        filename=filename,
        frame_dirname=frames_dir,
        output_dirname=results_dir,
    )

    at.convert_to_gif(
        dirname=results_dir,
        filename=filename,
    )


def illustrate(signal, kernel, name, pad=None, value=0):
    signal = np.array(signal, dtype=float)
    kernel = np.array(kernel, dtype=float)

    # Pad the signal
    n_half_kernel = int((kernel.size - 1) / 2)
    if pad == "constant":
        signal = np.pad(
            signal,
            (n_half_kernel, n_half_kernel),
            constant_values=value)
    elif pad == "mirror":
        head = signal[:n_half_kernel]
        tail = signal[-n_half_kernel:]
        signal = np.concatenate((head[::-1], signal, tail[::-1]))
    elif pad == "circular":
        signal = np.concatenate(
            (signal[-n_half_kernel:],
                signal,
                signal[:n_half_kernel]))
    elif pad == "none":
        pass

    conv_video(signal, kernel, name, fps=8)
    conv_diagram(
        signal,
        kernel,
        signal.size - kernel.size + 1,
        output_directory_name=results_dir,
        filename_stem=name,
        show_math=False,
    )


# signal = [
#     .8, .5, .9, -.1, .7, .6, 1, .3, .9, .4, -.3, .2, -.1,
#     .4, -.2, -.8, -.6, -.9, .1, -.1, -.3, -1, -.8, -.5, -.9,
# ]
# kernel = [.1, .3, .6, .8, 1, .8, .6, .3, .1]
# illustrate(signal, kernel, "test", pad="constant", value=0)
