import os
import pickle as pkl
import numpy as np
import matplotlib.pyplot as plt
import cottonwood.core.toolbox as tb
import cottonwood.examples.convnet.conv1d_viz as conv_viz
from cottonwood.core.logger import ConfusionLogger
from ecg_data_block_eval import AllData


reports_dir = os.path.join("reports", tb.date_string())
correct_dir = os.path.join(reports_dir, "correctly_predicted")
incorrect_dir = os.path.join(reports_dir, "incorrectly_predicted")
os.makedirs(correct_dir)
os.makedirs(incorrect_dir)
filename = "heartbeat_classifier.pkl"
with open(filename, "rb") as f:
    classifier = pkl.load(f)

classifier.add(AllData(), "all_data")
classifier.connect("all_data", "convolution_0", i_port_tail=0)
classifier.connect("all_data", "one_hot", i_port_tail=1)

confusion_logger = ConfusionLogger(
    reports_path=reports_dir,
    verbose=True,
)
plot_counts_correct = np.zeros(4)
n_correct_plots = 20
i_miss = 1000

while True:
    try:
        classifier.forward_pass()
    except StopIteration:
        break

    i_predicted = int(np.argmax(classifier.blocks["hard_max"].result))
    i_actual = int(np.argmax(classifier.blocks["one_hot"].result))
    labels = classifier.blocks["one_hot"].get_labels()
    predicted_label = labels[i_predicted]
    actual_label = labels[i_actual]
    data = classifier.blocks["all_data"].current[0]

    # Plot all the incorrect predictions
    if i_predicted != i_actual:
        filename = f"{actual_label}_as_{predicted_label}_{i_miss}.png"
        fig = plt.figure()
        ax = fig.add_subplot(2, 1, 1)
        ax.plot(data[0, :])
        ax.plot(data[1, :])
        ax.set_title(f"Classified {actual_label} as {predicted_label}")
        ax_d = fig.add_subplot(2, 1, 2)
        ax_d.plot(data[2, :])
        ax_d.plot(data[3, :])
        fig.savefig(os.path.join(incorrect_dir, filename))
        plt.close()

        i_miss += 1

    # Plot some of the correct predictions
    else:
        if plot_counts_correct[i_actual] < n_correct_plots:
            filename = f"{actual_label}_{plot_counts_correct[i_actual]}.png"
            fig = plt.figure()
            ax = fig.add_subplot(2, 1, 1)
            ax.plot(data[0, :])
            ax.plot(data[1, :])
            ax.set_title(f"Correctly classified {actual_label}")
            ax_d = fig.add_subplot(2, 1, 2)
            ax_d.plot(data[2, :])
            ax_d.plot(data[3, :])
            fig.savefig(os.path.join(correct_dir, filename))
            plt.close()
            plot_counts_correct[i_actual] += 1

    confusion_logger.log_values(
        classifier.blocks["hard_max"].result,
        classifier.blocks["one_hot"].result,
        labels)

classifier.remove("all_data")

confusion_matrix_stats = confusion_logger.calculate_stats()
mean_recall = np.mean(confusion_matrix_stats[:, 3])
tuning_loss = 1 - mean_recall

conv_viz.render(
    classifier.blocks["convolution_0"],
    reports_dir,
    f"conv_0.png")
conv_viz.render(
    classifier.blocks["convolution_1"],
    reports_dir,
    f"conv_1.png")
