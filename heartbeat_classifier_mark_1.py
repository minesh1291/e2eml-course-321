import os
import numpy as np
from cottonwood.core.blocks.activation import Logistic, ReLU
from cottonwood.core.blocks.conv1d import Conv1D
from cottonwood.core.blocks.linear import Linear
from cottonwood.core.blocks.loss import MeanSquareLoss
from cottonwood.core.blocks.operations import Difference, Flatten, OneHot
from cottonwood.core.blocks.pooling import MaxPool1D
from cottonwood.core.blocks.structure import Structure
import cottonwood.core.toolbox as tb
import cottonwood.examples.convnet.conv1d_viz as conv_viz
import cottonwood.examples.simulation.visualize_structure as struct_viz
from cottonwood.core.logger import ValueLogger
# from cottonwood.experimental.blocks.normalization \
#     import OnlineBatchNormalization as BatchNormalization
from ecg_data_block import TrainingData, TuningData, TestingData


def train(verbose=False):

    reports_dir = os.path.join("reports", tb.date_string())
    if verbose:
        os.makedirs(reports_dir, exist_ok=True)

        msg = f"""

    Training convolutional neural network on the ECG data set.
    Look for documentation and visualizations
    in the {reports_dir} directory.

    """
        print(msg)

    n_training_iter = int(3e5)
    n_tuning_iter = int(3e4)
    n_report_interval = int(1e4)
    n_viz_interval = int(1e5)

    classifier = Structure()
    classifier.add(TrainingData(), "training_data")

    # Add blocks for the classification branch of the network
    kernel_size = 5
    n_kernels = 15
    classifier.add(
        Conv1D(kernel_size=kernel_size, n_kernels=n_kernels),
        "convolution_0")
    classifier.add(ReLU(), "relu_0")
    classifier.add(MaxPool1D(), "max_pool_0")
    classifier.add(Flatten(), "flatten")
    classifier.add(Linear(4), "linear")
    classifier.add(Logistic(), "logistic")

    # Add blocks for the ground truth comparison branch
    classifier.add(OneHot(4), "one_hot")
    classifier.add(Difference(), "difference")
    classifier.add(MeanSquareLoss(), "mean_sq_loss")

    # Create all the connections between the blocks
    classifier.connect("training_data", "convolution_0", i_port_tail=0)
    classifier.connect("training_data", "one_hot", i_port_tail=1)

    classifier.connect("convolution_0", "relu_0")
    classifier.connect("relu_0", "max_pool_0")
    classifier.connect("max_pool_0", "flatten")
    classifier.connect("flatten", "linear")
    classifier.connect("linear", "logistic")

    classifier.connect("logistic", "difference", i_port_head=0)
    classifier.connect("one_hot", "difference", i_port_head=1)
    classifier.connect("difference", "mean_sq_loss")

    loss_logger = ValueLogger(
        value_name="loss",
        log_scale=True,
        report_min=-3,
        report_max=0,
        reports_path=reports_dir,
        reporting_bin_size=n_report_interval,
        verbose=verbose,
    )

    if verbose:
        struct_viz.render(classifier, reports_dir)

    # Execute the training loop
    for i_iter in range(n_training_iter):
        classifier.forward_pass()
        classifier.backward_pass()
        loss_logger.log_value(classifier.blocks["mean_sq_loss"].loss)
        if verbose:
            if (i_iter + 1) % n_viz_interval == 0:
                conv_viz.render(
                    classifier.blocks["convolution_0"],
                    reports_dir,
                    f"conv_0_{i_iter + 1:07}.png")
    if verbose:
        tb.summarize(classifier, reports_dir=reports_dir)

    classifier.remove("training_data")
    classifier.add(TuningData(), "tuning_data")
    classifier.connect("tuning_data", "convolution_0", i_port_tail=0)
    classifier.connect("tuning_data", "one_hot", i_port_tail=1)

    # Execute the loop evaluating performance on the tuning data
    for i_iter in range(n_tuning_iter):
        classifier.forward_pass()
        loss_logger.log_value(classifier.blocks["mean_sq_loss"].loss)

    tuning_loss = np.mean(loss_logger.value_history[n_training_iter:])
    return classifier, tuning_loss, loss_logger.value_history


if __name__ == "__main__":
    model, error = train(verbose=True)
    print(error)
