"""
Animate the convolution of one-dimensional signals with kernels.

The convention used here is that the kernel is the shorter of the two, but
the math doesn't care which is which.

This makes use of the Lodgepole image processing package:
https://gitlab.com/brohrer/lodgepole

and FFMPEG:
https://ffmpeg.org/

parameter dictionary
All are floats, in units of centimeters
----------
hgt: figure height
wid: figure width
hbrd: horizontal border width
lblw: label column width
math: height of math text
pntw: width of default spacing between points
sigh: height of the signal plots
sigw: width of the signal plots
vbrd: vertical border width
"""
import os
import numpy as np
import matplotlib.pyplot as plt
import lodgepole.animation_tools as at
plt.switch_backend("agg")


# Valid Matplotlib colors. More examples here:
# https://e2eml.school/matplotlib_lines.html#color
canvas_color = "#fbf9f6"  # warm white
kernel_color = "#050560"
signal_color = "#600505"
result_color = "#04253a"  # dark blue
math_color = "#a0a0a0"  # light gray
print_color = "#04253a"

dpi = 300  # Image resolution, dots per inch
results_dir = "results"
frames_dir = "frames"

# Ensure that the necessary directories exist
try:
    os.mkdir(results_dir)
except FileExistsError:
    pass
try:
    os.mkdir(frames_dir)
except FileExistsError:
    pass


def get_empty_frame(
    fig_width=16,  # centimeters
    fig_height=9,  # centimeters
    canvas_color="white",
    border_color="black",
):
    """
    Generate a blank canvas on which to draw
    """
    # figsize expects inches. Convert from centimeters.
    fig = plt.figure(figsize=(fig_width / 2.54, fig_height / 2.54))

    # Cover the entire figure with an Axes object
    ax = fig.add_axes((0, 0, 1, 1))

    # Ensure that 1 unit in the Axes is 1 centimeter in the final image
    ax.set_xlim(0, fig_width)
    ax.set_ylim(0, fig_height)

    ax.set_facecolor(canvas_color)

    # Clean up the axes
    ax.tick_params(
        bottom=False,
        top=False,
        left=False,
        right=False)
    ax.tick_params(
        labelbottom=False,
        labeltop=False,
        labelleft=False,
        labelright=False)

    # Create a border
    ax.spines["top"].set_color(border_color)
    ax.spines["bottom"].set_color(border_color)
    ax.spines["left"].set_color(border_color)
    ax.spines["right"].set_color(border_color)
    ax.spines["top"].set_linewidth(4)
    ax.spines["bottom"].set_linewidth(4)
    ax.spines["left"].set_linewidth(4)
    ax.spines["right"].set_linewidth(4)

    return fig, ax


def calculate_parameters(n_sig, n_ker):
    # Overall figure size
    wid = 16
    hgt = 9

    # A little whitespace around the edges
    hbrd = .5
    vbrd = .5

    # Some room for text labels on the right hand edge
    lblw = .75

    # Some room for math symbols between plots
    math = .4

    # Calculate the size of the plots
    sigh = (hgt - vbrd * 2 - math * 3) / 4
    sigw = wid - hbrd * 2 - lblw

    # Calculate the spacing between points
    pntw_min = .5  # The minimum spacing allowed between points
    pntw = np.minimum(pntw_min, sigw / (n_sig + 2 * (n_ker - 1)))

    return (wid, hgt, hbrd, lblw, math, pntw, sigw, sigh, vbrd)


def plot_lolli(ax, arr, x_center, y, pntw, sigh, color="black"):
    """
    Create a lollipop-style plot of a one-dimensional array
    """
    width = arr.size * pntw
    x_min = x_center - width / 2
    x_max = x_center + width / 2

    # Scale the array to fill the allotted vertical space.
    # Assumes that the arr has a maximum magnitude of 1
    arr *= sigh / 2

    ax.plot([x_min, x_max], [y, y], color=color, linewidth=1)
    for i, val in enumerate(arr):
        xpt = x_min + (i + .5) * pntw
        ypt = y + val
        ax.plot([xpt, xpt], [y, ypt], color=color, linewidth=.5)
        ax.scatter(
            xpt, ypt,
            marker="o",
            edgecolor=color,
            facecolor=color,
            s=20)


def conv_diagram(
    signal,
    kernel,
    position,
    n_reps=1,
    dirname=".",
    namebase=None,
    show_math=True,
):
    """
    Create an image showing the convolution.
    """
    if namebase is None:
        namebase = "frame"

    # Scale to have maximum magnitude of 1
    # This makes for a good visual presentation
    kernel /= np.max(np.abs(kernel))
    signal /= np.max(np.abs(signal))

    wid, hgt, hbrd, lblw, math, pntw, sigw, sigh, vbrd = (
        calculate_parameters(signal.size, kernel.size))

    fig, ax = get_empty_frame(
        fig_width=wid,
        fig_height=hgt,
        canvas_color=canvas_color,
        border_color=print_color,
    )

    def place_label(y, label):
        x_label = wid - hbrd - lblw / 2
        ax.text(
            x_label, y,
            label,
            horizontalalignment="center",
            verticalalignment="center",
            rotation=90,
            color=print_color,
            fontsize=8,
        )

    # Plot the kernel
    y_ker = vbrd + 3.5 * sigh + 3 * math
    x_center = hbrd + .5 * sigw
    plot_lolli(ax, kernel, x_center, y_ker, pntw, sigh, color=kernel_color)
    place_label(y_ker, "kernel")

    # Plot the signal
    y_sig = vbrd + 2.5 * sigh + 2 * math
    plot_lolli(ax, signal, x_center, y_sig, pntw, sigh, color=signal_color)
    place_label(y_sig, "signal")

    # Plot the sliding kernel
    sig_left = x_center - ((signal.size - 1) * pntw) / 2
    sig_pos = sig_left + pntw * position
    x_ker_center = sig_pos - ((kernel.size - 1) * pntw) / 2
    y_ker = vbrd + 1.5 * sigh + math
    plot_lolli(
        ax,
        kernel[::-1],
        x_ker_center,
        y_ker,
        pntw,
        sigh,
        color=kernel_color,
    )
    place_label(y_ker, "convolution")

    # Plot the result
    res = np.convolve(signal, kernel, mode="full")
    res /= np.max(np.abs(res))
    resw = (position + 1) * pntw
    x_res_center = sig_left - (kernel.size * pntw - resw) / 2
    y_res = vbrd + .5 * sigh
    plot_lolli(
        ax,
        res[:position + 1],
        x_res_center,
        y_res,
        pntw,
        sigh,
        color=result_color,
    )
    place_label(y_res, "result")

    if show_math:
        # Show the multiplications
        sig_right = sig_left + (signal.size - 1) * pntw
        y_mult = vbrd + 2 * sigh + 1.5 * math
        for k in range(kernel.size):
            x_mult = sig_pos - k * pntw
            # Add an epsilon to account for floating point errors
            if x_mult >= (sig_left - 1e-6) and x_mult <= (sig_right + 1e-6):
                ax.text(
                    x_mult,
                    y_mult,
                    "X",
                    fontsize=6,
                    color=math_color,
                    horizontalalignment="center",
                    verticalalignment="center",
                )
                # This line extends down toward the addition
                ax.plot(
                    [x_mult, x_mult],
                    [y_mult + .5 * sigh + .5 * math,
                        y_mult - sigh - .5 * math],
                    color=math_color,
                    linewidth=.5,
                    alpha=.5,
                    zorder=-4,
                )
                ax.plot(
                    [x_mult, x_mult],
                    [y_mult + math / 4, y_mult - math / 4],
                    color=canvas_color,
                    linewidth=.5,
                    zorder=-3,
                )

        # Show the addition
        y_add = vbrd + sigh + .5 * math
        x_add = sig_left + resw - ((kernel.size + 1) * pntw) / 2
        ax.text(
            x_add,
            y_add,
            "+",
            fontsize=8,
            color=math_color,
            horizontalalignment="center",
            verticalalignment="center",
        )
        ax.plot(
            [x_add, x_add],
            [y_add - .5 *  math, y_add - .5 * (sigh + math)],
            color=math_color,
            linewidth=.5,
            alpha=.5,
            zorder=-4,
        )

    # Generate the images to be used as video frames
    # Simulate a slower frame rate by creating copies of each image
    for i_frame in np.arange(position * n_reps, (position + 1) * n_reps):
        imagename = namebase + str(int(10000 + i_frame)) + ".png"
        fig.savefig(os.path.join(dirname, imagename), dpi=dpi)

    plt.close()


def conv_video(sig, ker, filename_base, fps=30):
    """
    Animate the process of convolution
    """
    # Clean out previous frames
    files = os.listdir(frames_dir)
    for f in files:
        os.remove(os.path.join(frames_dir, f))
    # Calculate how many times each image needs to be repeated to get
    # the desired frame rate
    n_frame_reps = int(np.ceil(30 / fps))

    filename = filename_base + ".mp4"
    signal = np.array(sig, dtype=float)
    kernel = np.array(ker, dtype=float)
    n_conv = signal.size + kernel.size - 1

    # Step through the convolution
    for position in np.arange(n_conv):
        conv_diagram(
            signal,
            kernel,
            position,
            n_reps=n_frame_reps,
            dirname=frames_dir,
        )

    at.render_movie(
        filename=filename,
        frame_dirname=frames_dir,
        output_dirname=results_dir,
    )

    at.convert_to_gif(
        dirname=results_dir,
        filename=filename,
    )


def illustrate(signal, kernel, name):
    signal = np.array(signal, dtype=float)
    kernel = np.array(kernel, dtype=float)
    conv_video(signal, kernel, name, fps=8)
    conv_diagram(
        signal,
        kernel,
        signal.size + kernel.size - 2,
        dirname=results_dir,
        namebase=name,
        show_math=False,
    )


signal = [1, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, .5, 0, 0, 0, 0, 0, -.5]
kernel = [1, 0, -.5, -1]
illustrate(signal, kernel, "test")
